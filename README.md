# FASTAPI

[FastAPI](https://fastapi.tiangolo.com/) is a Web framework for developing RESTful APIs in Python. FastAPI is based on Pydantic and type hints to validate, serialize, and deserialize data, and automatically auto-generate OpenAPI documents.

The simplest FastAPI file could look like this:

```python
# main.app
from fastapi import FastAPI

app = FastAPI()

@app.get("/")
async def root():
    return {"message": "Hello World"}
```

to start:

```bash
cd app
python main.py
```

FastAPI generates automatically the OpenApi documentation in swagger, redoc and in json:

- http://127.0.0.1:8000/docs
- http://127.0.0.1:8000/redoc
- http://127.0.0.1:8000/openapi.json

## SSH Tunneling from Raspberry Pi

```bash
ssh -N -L 8080:localhost:8000 rober@192.168.0.124
```

## Deploy in Space (Deta)

The config for `space deta` should be in the root.
All files should be in the `/app` folder, including a `.venv` folder, with a `requirements.txt`

https://deta.space

To push changes:
make sure you are in the project root, where the `Spacefile` is:

```
space push
```

### Environment Variables

Deta has already a .env in the server side.
In order to get the env variables in production:

```python
import os
deta_app_hostname: str = os.getenv("DETA_SPACE_APP_HOSTNAME")
    deta_app_micro_name:str = os.getenv("DETA_SPACE_APP_MICRO_NAME")
```

If you want to use a custom env variable, add a preset in the `.spacefile`:

````
# Spacefile Docs: https://go.deta.dev/docs/spacefile/v0

v: 0
micros:
  - name: rss-parser
    src: ./app/
    engine: python3.9
    presets:
      env:
        - name: SECRET_MESSAGE
          description: Secret message only available to this Micro
          default: 'deta is cool'
```

```
>>> from deta import Deta
>>> deta = Deta("e0WtpsgR1Koz_3E2x8CkPUw7zdYyrNxWyz9qBS18JuZyu")
>>> drive = deta.Drive("rss-parser")
>>> f = open('./data/data.json','r')
>>> drive.put('sentinel.json',f)
'sentinel.json'
>>> f.close()
```


## Requirements

- call directories using local path
- store pub backlog in sqlite if consumer not pushing
- Home page with links to documentation
- Home page to edit sites to be scrapped
- Authorization and Authentication
- Public IP and Domain
- Kafka integration
````
