# main.py

from pathlib import Path
from contextlib import asynccontextmanager
from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware

#import uvicorn
# uvicorn main:app --reload
from model.scheduler import site, scheduler
from routers import routes, pages

BASE_DIR = Path('./main.py').resolve().parent

@asynccontextmanager
async def lifespan(app: FastAPI):
    # Start the scheduled task scheduler
    scheduler.start()
    yield

app = FastAPI(lifespan=lifespan)
app.mount("/static", StaticFiles(directory=Path(BASE_DIR, 'static')), name="static")

origins = [
    "http://localhost:8080",
    "http://127.0.0.1:8080/",
    "https://rss-webapp.onrender.com",
    

]

app.include_router(routes.route)
app.include_router(pages.pages)
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=False,
    allow_methods=["*"],
    allow_headers=["*"],
)

# if __name__ == "__main__":
#     uvicorn.run("main:app", host="0.0.0.0", port=8050)

# from ./app run command line below
# uvicorn main:app --reload