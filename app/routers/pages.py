from pathlib import Path

from fastapi import Request, APIRouter
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates

BASE_DIR = Path('./main.py').resolve().parent
pages = APIRouter()
templates = Jinja2Templates(directory=Path(BASE_DIR, 'templates'))


@pages.get("/", response_class=HTMLResponse)
def index(request: Request):
    context = {'request': request}
    return templates.TemplateResponse("index.html", context)