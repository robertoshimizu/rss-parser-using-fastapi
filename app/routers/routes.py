# router.py
from fastapi import HTTPException, WebSocket, APIRouter
import asyncio
import json
from pubsub import pub
from model.rssParsers import RSSFeeder



# RSSFeeder is a Singleton
parsa = RSSFeeder()
route = APIRouter()


def grava_feed(arg):
    f =open('./data/backlog.json','a',encoding = 'utf-8')
    json.dump(arg,f, indent=1)
    f.write(f',\n')
    f.close()

queue = []

# ------------ create a listener ------------------

def listener1(arg1, arg2=None):
    print('Function listener1 received:')
    print('  arg1 =', arg1)
    # print('  arg2 =', arg2)
    # print()
    try:
        queue.append(arg2)
        grava_feed(arg2)
    except Exception as err:
        pass


    
    

pubListener, first = pub.subscribe(listener1, 'rootTopic')
print(pubListener.name())

@route.get("/sentinel")
async def sentinel():
    """
     Sentinel is the last record of each news source.
     It helps to filter out records already sent.
     Sentinel is saved in the folder /data/data.json
     In Deta, it is saved in a deta drive.
    """
    out = parsa.retrieve_sentinel()
    return out


@route.get("/allfeeds")
async def feed():
    """
     Capture all records in the most recent rss feed of each source
    """
    return parsa.allList()

@route.get("/newfeeds")
async def newsfeed():  
    """
     Retrieve only records between the sentinel and the most recent rss feed of each source.

    """  
    return parsa.narrowList()

@route.get("/feedsummaries")
async def summarize_feeds():
    """
     Summarize using newspaper3k the most recent list of feeds
    """  
    return parsa.summarizedList()


@route.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    while True:
        data = await websocket.receive_text()
        print(data)
        await websocket.send_json(f"{data}")


@route.websocket("/feedstream")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    data = iter(parsa.narrowList())

    while True:
        await asyncio.sleep(2)
        try:
            payload = next(data)
        except:
            pass
        else:
            await websocket.send_json(payload)



@route.websocket("/scheduler")
async def websocket_endpoint(websocket: WebSocket):
    try:
        await websocket.accept()
    except Exception as error:
            raise HTTPException(status_code=501, detail=error)
    
    while True:        
        await asyncio.sleep(2)
        if (queue.__len__() > 0):
            try:
                payload = queue.pop(0)                
                await websocket.send_json(payload)
                
            except Exception as error:
                raise HTTPException(status_code=501, detail=error)
            
            

