import socket
from utils.meta import Singleton

hostname = socket.gethostname()
hostdns = socket.getfqdn()

class Environment(metaclass=Singleton):
    def __init__(self) -> None:
        self.environ = hostname
        self.hostdns = hostdns
        
    
    def get_environ(self):
        return dict(host1=self.environ, host2=self.hostdns)
