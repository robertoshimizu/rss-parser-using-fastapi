from pykafka.exceptions import SocketDisconnectedError, LeaderNotAvailable
from pykafka import KafkaClient
from datetime import datetime
import json

from utils.meta import Singleton

class KafkaProducer(metaclass=Singleton):
    def __init__(self, topic) -> None:
        self.client = KafkaClient("192.168.0.181:9093",broker_version="1.0.0")
        self.topic = self.client.topics[topic]
        print('KafkaProducer initialized')

    def send_message(self, message):
        with self.topic.get_producer(delivery_reports=True) as producer:
            producer.produce(("Message passing thru kafka ").encode("ascii"))
            #jd = json.dumps(message).encode(encoding = 'UTF-8')
            #producer.produce(jd, timestamp=datetime.now())
            try:
                msg, exc = producer.get_delivery_report(block=False)
                if exc is not None:
                    print('Failed to deliver msg {}: {}'.format(
                        msg.partition_key, repr(exc)))
                else:
                    print('Successfully delivered msg {}'.format(
                    msg.partition_key))
            except: print()