# rssParsers.py
from typing import List
from urllib.parse import urlparse, parse_qs
import feedparser
from newspaper import Article, Config
import json
import time
import os
import io
from model.entities import Sentinel
from utils.dependencies import currentTime
from utils.meta import Singleton
from pathlib import Path
from fastapi import Depends, HTTPException

BASE_DIR = Path('./main.py').resolve().parent



#nltk.download('punkt')
user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:78.0) Gecko/20100101 Firefox/78.0'
config = Config()
config.browser_user_agent = user_agent
config.request_timeout=15
config.number_threads=20


magazines = [
    {
        'name': 'TechCrunch',
        'url': 'https://techcrunch.com/feed/'
    },
    {
        'name': 'Revista Apólice',
        'url': 'https://www.revistaapolice.com.br/feed/'
    },

    {
        'name': 'Sonho Seguro',
        'url': 'https://www.sonhoseguro.com.br/feed/'
    },
    {
        'name': 'InfoMoney',
        'url': 'https://www.infomoney.com.br/feed/'
    }
]


def getURL(nome):
    for mag in magazines:
        if (mag['name'] == nome):
            return mag['url']


list = [x['url'] for x in magazines]




class RSSFeeder(metaclass=Singleton):
    def __init__(self):
        self.name = 'Feeder Man'
        self.list = list
        print('RSSParser class initialized')
        self.sentinels:List[Sentinel] = [self.set_newSentinel(self.get_content(item)) for item in self.list]

    def parse(self):
        feeds = feedparser.parse('https://www.revistaapolice.com.br/feed/')
        return iter(feeds.entries)

    def parte(self, url):
        feeds = feedparser.parse(url)
        return feeds.entries

    def get_content(self, name):
        content = feedparser.parse(name)
        return content

    def save_sentinels(self):
        jsonString = json.dumps(self.sentinels)        
        env = 'dev'
        if env=='dev':
            jsonFile = open(f"{BASE_DIR}/data/data.json", "w")
            jsonFile.write(jsonString)
            jsonFile.close()
            print("Sentinels saved in data.json")
                  

    def get_sentinelsFromFile(self):
        fileObject = open(f"{BASE_DIR}/data/data.json", "r")
        jsonContent = fileObject.read()
        fileObject.close()
        aList = json.loads(jsonContent)

        for i in range(len(aList)):
            aList[i]['last'] = time.struct_time(
                aList[i]['last'])
        return aList

    def set_newSentinel(self, content):
        recent = content.entries[0]
        keys = ['name', 'Last_Updated', 'Num_Articles', 'Last_Article_Title', 'Last_Article_Published',
                'last']
        values = [content.feed.title, content.feed.updated, len(content.entries), recent.title, recent.published,
                  recent.published_parsed]
        sentinel = dict(zip(keys, values))
        return sentinel

    def execute(self):
        print('This parse has processed: ', len(self.list))

        print(self.sentinels)
        self.save_sentinels(self.sentinels)

        
    def retrieve_sentinel(self):
        env = 'dev'
        if env=='dev':
            sentinels_fromFile = self.get_sentinelsFromFile()
        return sentinels_fromFile
        
    def narrowList(self):
        narrowLista = []
        self.sentinels = [self.set_newSentinel(self.get_content(item)) for item in self.list]
        sentinels_old:List[Sentinel] = self.retrieve_sentinel()        
        
        check = zip(sentinels_old, self.sentinels)
        
        try:
            for item in check:
                a, b = item
                if (a != b):
                    url = getURL(a['name'])
                    entries = self.parte(url)
                    lista = [x for x in entries if x['published_parsed'] > a['last']]
                    if len(lista)>0:
                        print(a['name'])
                        narrowLista.append(lista)
        except:
            raise HTTPException(status_code=501, detail="Comparing Sentinels")
        
        if len(narrowLista) > 0:
            narrowLista = [val for sublist in narrowLista for val in sublist]
            narrowLista.sort(key=lambda article: time.struct_time(article["published_parsed"]))
            try:
                self.save_sentinels()
            except:
                raise HTTPException(status_code=501, detail="Sentinel Cannot be saved")
            return narrowLista
        else:
            return narrowLista

    def summarizedList(self):
        newList = self.narrowList()
        out = []
        keys=['title','link','author', 'published', 'published_parsed',  'summary_3k']
        print(f'Iniciando Newspaper3k at {currentTime()}')
        if len(newList)>0:
            for item in newList:
                url = item["link"]
                #url = urlparse(url)
                #print(parse_qs(url.query)['url'][0])
                # print(item)
                # print()

                title = item["title"]
                author= item["author"]
                published = item["published"]
                published_parsed = item["published_parsed"]

                # Newspaper3k parsing
                try:
                    article = Article(url, config=config)
                    article.download()
                    article.parse()
                    article.nlp()
                    summary_3k = article.summary
                except Exception as err:
                    print(f'Erro no download de {url}')
                    print(err)
                    summary_3k=''
                time.sleep(1)
                out.append(dict(zip(keys,[title, url, author, published, published_parsed, summary_3k])))
            print(f'Finalizando Newspaper3k at {currentTime()}')
            return out
        else:
            return []

    def allList(self):
        allLista = []
        self.sentinels = [self.set_newSentinel(self.get_content(item)) for item in self.list]
        for item in self.sentinels:
            print(item['name'])
            url = getURL(item['name'])
            entries = self.parte(url)
            allLista.append(entries)
        allLista = [val for sublist in allLista for val in sublist]
        allLista.sort(key=lambda article: time.struct_time(article["published_parsed"]))
        return allLista


if __name__ == "__main__":
    print("Test News Feed Parsing")
