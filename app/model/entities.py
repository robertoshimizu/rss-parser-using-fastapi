from datetime import datetime
from typing import Union
from pydantic import BaseModel


class Sentinel(BaseModel):
    name: str
    Last_Updated:str
    Num_Articles:int
    Last_Article_Title: str
    Last_Article_Published: str
    last: datetime