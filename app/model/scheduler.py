# scheduler.py
"""
FastAPI-Scheduler is a simple scheduled task management FastAPI extension library based on APScheduler.

Documentation: https://github.com/amisadmin/fastapi_scheduler

Open http://127.0.0.1:8000/admin/ in your browser to see the UI preview

"""
#from pykafka.exceptions import SocketDisconnectedError, LeaderNotAvailable
#from pykafka import KafkaClient


from fastapi_amis_admin.admin.settings import Settings
from fastapi_amis_admin.admin.site import AdminSite
from fastapi_scheduler import SchedulerAdmin
import time
from utils.dependencies import currentTime
from model.rssParsers import RSSFeeder

# Create `AdminSite` instance
from pubsub import pub

#from data.kafkacon import KafkaProducer

site = AdminSite(settings=Settings(database_url_async='sqlite+aiosqlite:///amisadmin.db'))

# Create an instance of the scheduled task scheduler `SchedulerAdmin`
scheduler = SchedulerAdmin.bind(site)
parsa = RSSFeeder()

# kafka

#client = KafkaClient("192.168.0.181:9093",broker_version="1.0.0", zookeeper_hosts="192.168.0.181:2181")
####topic = client.topics["Caraio"]
#producer = topic.get_producer()


# Add scheduled tasks, refer to the official documentation: https://apscheduler.readthedocs.io/en/master/
# use when you want to run the job at fixed intervals of time
# @scheduler.scheduled_job('interval', minutes=10)
# def interval_task_test():
#     now = datetime.now()
#     current_time = now.strftime("%H:%M:%S")
#     print('-----------------------------------------------------')
#     message = f'Interval initiated at {current_time}'
#     print(message)
#     data = iter(parsa.narrowList())
#
#     # consume iterator
#     while True:
#         try:
#             # get the next item
#             element = next(data)
#             pub.sendMessage('rootTopic', arg1=element['id'], arg2=element)
#             # do something with element
#         except StopIteration:
#             # if StopIteration is raised, break from loop
#             print('Interval task finished')
#             print('')
#             break

@scheduler.scheduled_job('interval', seconds=180, max_instances=2)
def producer():   
    #producer.start()
    print('-----------------------------------------------------')

    print(f'Interval task initiated at {currentTime()}')
    #data = iter(parsa.narrowList())
    data = iter(parsa.summarizedList())

    # consume iterator
    while True:
        try:
            # get the next item
            element = next(data)
            pub.sendMessage(topicName='rootTopic', arg1=element['title'], arg2=element)
            # send it to kafka
            #jd = json.dumps(element).encode(encoding = 'UTF-8')
            #producer.produce(jd, timestamp=datetime.now()

            time.sleep(1)
            # do something with element
        except StopIteration:
            # if StopIteration is raised, break from loop
            print(f'Interval task finished at {currentTime()}')
            print('')
            break

    #producer.stop()


# use when you want to run the job periodically at certain time(s) of day
#@scheduler.scheduled_job('cron', hour=3, minute=30)
#def cron_task_test():
#    print('cron task is run...')


# use when you want to run the job just once at a certain point of time
#@scheduler.scheduled_job('date', run_date=date(2022, 11, 16))
#def date_task_test():
#    print('date task is run...')


